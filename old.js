const firebase = require('firebase');
const jsonfile = require('jsonfile');

file = './config.json';
localConfig = jsonfile.readFileSync(file);

firebaseConfig = {
    apiKey: "AIzaSyAT5AhtwLv7nk95IJgB3YWyCtozSSglcQQ",
    authDomain: "frikbot-dd7c3.firebaseapp.com",
    databaseURL: "https://frikbot-dd7c3.firebaseio.com",
    projectId: "frikbot-dd7c3",
    storageBucket: "frikbot-dd7c3.appspot.com",
    messagingSenderId: "605383453611"
  };

var fire = firebase.initializeApp(firebaseConfig);

var db = firebase.database(fire);

var users = db.ref('users');

var firebaseI = {};

firebaseI.checkUser = function (userData) {
  db.ref('users/'+ userData.name)
}

function updateUser(name) {
  db.ref('users/' + name).update({
    coins: coins + 1
  });
}

firebaseI.addUsers = function(datos) {
  db.ref('users').orderByKey().equalTo(datos).once('value', function(data) {
    ch = data.val();
    if (ch == null) {
      db.ref('users').update({
        [datos]: {
          username: datos,
          coins: 1
        }
      });
    } else {
      for (name in ch) {
        coins = ch[name].coins;
      }
      db.ref('users/' + name).update({
        coins: coins + 1
      });
    }

  });
};

/*firebase.bonus = function (username, cantidad) {
  db.ref('users/'+username).once('value', function (data) {
    coins = coins;
    db.ref('users/'+username).update({ coins: coins+cantidad});
  });
}*/

firebaseI.bon = function(username, cantidad) {
  db.ref('users/' + username).once('value', function(data) {
    userInf = data.val();
    coins = userInf.coins;
    cantidad = JSON.parse(cantidad);
    bonus = coins + cantidad;
    db.ref('users/' + username).update({coins: bonus});
  });
}

firebaseI.bonall = function(username, cantidad) {
  db.ref('users/' + username).once('value', function(data) {
    userInf = data.val();
    coins = userInf.coins;
    cantidad = JSON.parse(cantidad);
    bonus = coins + cantidad;
    db.ref('users/' + username).update({coins: bonus});
  });
}

firebaseI.checkdb = function() {
  db.ref('users').once('value', function(data) {
    console.log(data.val());
  });
}

firebaseI.checkCoins = function(username, callback) {
  db.ref('users/' + username).once('value', function(data) {
    userD = data.val();
    coins = userD.coins;

    callback(coins);
  });
};

firebaseI.addCoins = function(username, cantidad, callback) {
  db.ref('users/' + username).once('value', function(data) {
    da = data.val();
    coins = coins;
    newcoins = coins + (cantidad * 2);
    db.ref('users/' + username).update({coins: newcoins});
  });
}

firebaseI.remCoins = function(username, cantidad, callback) {
  db.ref('users/' + username).once('value', function(data) {
    da = data.val();
    coins = coins;
    db.ref('users/' + username).update({
      coins: coins - cantidad
    });
  });
}

firebaseI.make = function(opcion, valor, callback) {
  db.ref('apuestas/opciones').update({[opcion]: valor});
}

firebaseI.apuesta = function(opcion, cantidad, username, callback) {
  cantidad = JSON.parse(cantidad);
  db.ref('apuestas/apostadores').orderByKey().equalTo(username).once('value', function(data) {
    if (data.val()) {
      callback(1);
    } else {
      if (cantidad >= 5) {
          db.ref('users/' + username).once('value', function(data) {
            coins = data.val().coins;
            if (coins >= cantidad) {
              db.ref('users/' + username).update({
                coins: coins - cantidad
              });
              db.ref('apuestas/'+opcion).once('value', function (data) {
                if (data.val() == null) {
                  apos = 0;
                  fondo = 0;
                } else {
                  apos = data.val().apos;
                  fondo = data.val().fondo;
                }
                db.ref('apuestas/opciones/'+opcion).update({
                  apos: apos + 1,
                  fondo: fondo + cantidad,
                  apostadores: {
                    [username]: {
                      cantidad: cantidad,
                      username: username
                    }
                  }
                });
              });
              db.ref('apuestas/fondo').once('value', function (data) {
                if (data.val() == null) {
                  apos = 0;
                  fondo = 0;
                } else {
                  apos = data.val().apos;
                  fondo = data.val().fondo;
                }
                db.ref('apuestas/fondo').update({
                  apos: apos + 1,
                  fondo: fondo + cantidad
                });
              });
              db.ref('apuestas/apostadores').update({
                [username]: username
              });
              callback(2);
            } else {
              callback(false);
            }
          });
      } else {
        callback(3)
      }
    }
  });
}

firebaseI.gana = function (opcion) {
  db.ref('apuestas/').once('value', function (data) {
    info = data.val();
    db.ref('apuestas/opciones/'+opcion+'/apostadores').once('value', function (data) {
      dasd = data.val();
      console.log(dasd.length);
    });
  });
}

module.exports = firebaseI;
