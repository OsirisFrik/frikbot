# FRIKBOT

### Require 

 * Nodejs
 * NPM
 * Firebase 
 * Twitch APIKey

### Install

``` 
npm install 
```

Renombrar `` /keys/ejemplo.json `` -> `` /keys/keys.json ``
 * puerto predefinido es `` 3000 ``
 * obeten TwitchAPIKey en --> https://dev.twitch.tv
  * Al crear tu APIKey usa la URL `` http://localhost:port/auth/twitch/callback ``
  * incertalas en sus espacios corespondientes en `` keys.json ``
 * opten firebase app --> https://console.firebase.google.com
  * ve a `` Añade Firebase a tu aplicación web `` y copea tus llaves de acceso en `` keys.json ``
 * opten admin file de firebase en `` https://console.firebase.google.com/project/TUPROYECTO/settings/serviceaccounts/adminsdk ``
  * guarda el archivo en `` keys/ ``
  * remplaza el nombre del documento de `` adminKeys `` con el nombre de tu archivo
  
### Ejecutar

```
node app.js
```