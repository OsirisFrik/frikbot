// Define our dependencies
const express = require('express');
var app = express();
const http = require('http').Server(app);
const https = require('https');
const session = require('express-session');
const passport = require('passport');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const request = require('request');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const path = require('path');
const bot = require('./models/tmi.js');
const fireFun = require('./models/firebaseInit.js');
const jsonfile = require('jsonfile');
const admin = require('firebase-admin'); // Import Firebase Admin
const io = require('socket.io')(http);
const push = require('push.js');

var adminKeys = require('./keys/frikbot-dd7c3-firebase-adminsdk-7bhjj-3f907d3b34.json');
var keys = require('./keys/keys.json');
var customToken;
var login = false;

// init admin
admin.initializeApp({
  credential: admin.credential.cert(adminKeys),
  databaseURL: keys.firebase.databaseURL
});

io.on('connection', function (socket) {
  socket.on('login', function () {
    login = true;
  })
})

file = './keys/keys.json';
localConfig = jsonfile.readFileSync(file);

// Define our constants, you will change these with your own
const TWITCH_CLIENT_ID = keys.TWITCH_CLIENT_ID;
const TWITCH_SECRET = keys.TWITCH_SECRET;
const SESSION_SECRET = keys.SESSION_SECRET;
const CALLBACK_URL = keys.CALLBACK_URL+'/auth/twitch/callback'; // You can run locally with - http://localhost:3000/auth/twitch/callback

// Initialize Express and middlewares
app.use(session({
  secret: SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}));
app.use(express.static('public'));
app.use(passport.initialize());
app.use(passport.session());

// handlebars config
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
  defaultLayout: 'layout',
  //layoutsDir: "bot/views/layouts",
  extname: '.hbs'
}));
app.set('view engine', '.hbs');

// set static folder
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '/node_modules')));

// bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
// gobla vars
app.use(function(req, res, next) {
  if (req.session.passport && req.session.passport.user) {
    res.locals.user = req.session.passport.user;
  } else {
    res.locals.user = null;
  };
  next();
});

var checkSesion = function (req, res) {
  var check;
  if (req.session && req.session.passport && req.session.passport.user) {
    check = true;
  } else {
    check = false;
  }
  var promise = new Promise(function(resolve, reject) {
    if (check === true) {
      resolve(check)
    } else {
      reject(check)
    }
  });

  console.log(check);

  return promise;
}

// Override passport profile function to get user profile from Twitch API
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
  var options = {
    url: 'https://api.twitch.tv/kraken/user',
    method: 'GET',
    headers: {
      'Client-ID': TWITCH_CLIENT_ID,
      'Accept': 'application/vnd.twitchtv.v5+json',
      'Authorization': 'OAuth ' + accessToken
    }
  };

  request(options, function(error, response, body) {
    if (response && response.statusCode == 200) {
      done(null, JSON.parse(body));
    } else {
      done(JSON.parse(body));
    }
  });
}

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use('twitch', new OAuth2Strategy({
  authorizationURL: 'https://api.twitch.tv/kraken/oauth2/authorize',
  tokenURL: 'https://api.twitch.tv/kraken/oauth2/token',
  clientID: TWITCH_CLIENT_ID,
  clientSecret: TWITCH_SECRET,
  callbackURL: CALLBACK_URL,
  state: true
}, function(accessToken, refreshToken, profile, done) {
  profile.accessToken = accessToken;
  profile.refreshToken = refreshToken;

  // Securely store user profile in your DB
  //User.findOrCreate(..., function(err, user) {
  //  done(err, user);
  //});

  done(null, profile);
}));

// Set route to start OAuth link, this is where you define scopes to request
app.get('/auth/twitch', function(req, res, next) {
  next();
}, passport.authenticate('twitch', {
  scope: 'user_read'
}));

// Set route for OAuth redirect
app.get('/auth/twitch/callback', passport.authenticate('twitch', {
  successRedirect: '/bot',
  failureRedirect: '/'
}));

// If user has an authenticated session, display it, otherwise display link to authenticate
app.get('/', function(req, res) {
  if (req.session && req.session.passport && req.session.passport.user) {
    userInfo = req.session.passport.user;
    localConfig = jsonfile.readFileSync(file);
    if (localConfig.firebase != null) {
      res.render('index', {
        title: 'Home'
      });
    } else {
      res.redirect('/config');
    }
  } else {
    res.render('login', {
      title: 'Login',
      userPic: 'img/userDefault.png'
    });
  }
});

app.get('/config', function(req, res) {

});

app.get('/bot', function(req, res) {
  checkSesion(req, res).then(function (response) {
    admin.auth().createCustomToken(req.session.passport.user.name + '_' + req.session.passport.user._id).then(function(customToken) {
      userToken = customToken;
      fireFun.login(customToken, req.session.passport.user).then(function(response) {
        console.log('login >>',response);
        if (response) {
          bot.bot(req.session.passport.user);
          res.redirect('/')
        } else {
          res.send('ha ocurrido un error');
        }
      });
    }, function(err) {
      console.log(err);
    });
  }, function (err) {
    res.redirect('/');
  });
});

app.post('/config', function(req, res, next) {
  var saveConfig = {
    userData: userInfo,
    firebase: {
      apiKey: req.body.apiKey,
      authDomain: req.body.authDomain,
      databaseURL: req.body.databaseURL,
      projectId: req.body.projectId,
      storageBucket: req.body.storageBucket,
      messagingSenderId: req.body.messagingSenderId
    }
  };
  jsonfile.writeFileSync(file, saveConfig, {
    spaces: 2
  }, function(err) {
    console.error(err)
  });
  localConfig = jsonfile.readFileSync(file);
  checkBot(localConfig);
  res.render('postConfig', {
    title: 'config',
    helpers: {
      userPic: userInfo.logo,
      displayName: userInfo.display_name,
      canal: userInfo.name
    }
  });
});

app.get('/channel/:channel', function (req, res) {
  fireFun.getData(req.params.channel).then(function (response) {
    res.send(response);
  })
})

app.get('/bet', function(req, res) {
  res.render('bet', {
    title: 'Apuestas'
  });
});

app.get('/logout', function(req, res) {
  req.logout();
  res.render('logout');
});

http.listen(keys.port, function() {
  console.log('\x1b[35m%s\x1b[0m','frikbot esta corriendo en http://localhost:3003');
});
