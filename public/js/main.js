var socket = io.connect('http://localhost:3003', {'forceNew': true});

function sendSocket(name, data) {
  console.log(name);
  socket.emit(name, data);
}

socket.on('Bienvenido', function (data) {
  console.log(data);
  pushNotification(data);
});

socket.on('logeado', function (data) {
  console.log(data);
})

function inPage(page) {
  $('.item.red').removeClass('active');
  $(page).addClass('active');
}

function checkInput(section) {
  $(section).removeClass('disabled');
  $(section).addClass('green');
}
function changeChannel(section) {
  var messagingSenderId = $('[name="messagingSenderId"]').val();
  if (messagingSenderId != "") {
      checkInput(section);
  }
}

function loading() {
  document.getElementById('login').className += 'invisible';
  document.getElementById('loading').className = '';
  sendSocket('login', null);
}

Push.Permission.request();

function pushNotification(data) {
  Push.create('Bienvenido', {
    body: data.body,
    timeout: data.timeout,
    icon: '/img/3bsbot.png'
  });
}
