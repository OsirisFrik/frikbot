const firebase = require('firebase'); // Import Firebase
const jsonfile = require('jsonfile');

file = './keys/keys.json';
localConfig = jsonfile.readFileSync(file);

var fire = firebase.initializeApp(localConfig.firebase); // inicializa firebase

var db = firebase.database(fire); // database firebase

var firebaseI = {}; // modulo

// firebase rutas comunes
var routes = {
  usuarios: 'users/',
  canales: 'channels/',
  apuestas: 'bets/'
}

try {
// customToken Login
firebaseI.login = function(customToken, userData) {
  var loginResult = firebase.auth().signInWithCustomToken(customToken).then(function(response) {
    db.ref(routes.usuarios + userData.name).set(userData);
    return true;
  }, function(err) {
    return false;
    console.log(err);
  });
  var loginReturn = new Promise(function(resolve, reject) {
    if (loginResult) {
      resolve(loginResult);
    } else {
      reject(loginResult);
    }
  });
  return loginReturn;
}

// optener información del canal
firebaseI.getData = function(data) {
  console.log(data);
  db.ref(routes.usuarios + data).once('value', function(response) {
    channelData = response.val();
  });
  var channelInfo = new Promise(function(resolve, reject) {
    resolve(channelData);
  });
  return channelInfo;
}

// añadir coins/viewers automatico
firebaseI.addViewers = function(viewerInfo, idCanal, cantidad) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales + idCanal).orderByKey().equalTo(viewerInfo).once('value', function(data) {
      var viewers = data.val();
      if (!viewers) {
        db.ref(routes.canales + idCanal).update({
          [viewerInfo]: {
            viwer_name: viewerInfo,
            coins: 1
          }
        }).then(function () {
          resolve();
        });
      } else {
        for (var name in viewers) {
          coins = viewers[name].coins;
        }
        db.ref(routes.canales + idCanal + '/' + name).update({
          coins: coins + 1
        }).then(function () {
          resolve();
        });
      }
    }).catch(function (err) {
      console.log(err);
      reject();
    });
  });
}

// counsultar coins de viewer
firebaseI.checkCoins = function(username, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales + idCanal + '/' + username).once('value', function(response) {
      if (response.val()) {
        viewer = response.val();
        resolve(viewer.coins);
      } else {
        reject();
      }
    });
  });
};

// add coins to viewer
firebaseI.addCoins = function (viewer, cantidad, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales+idCanal+'/'+viewer).once('value', function (data) {
      userInfo = data.val();
      coins = userInfo.coins;
      give = coins + (cantidad * 2);
      db.ref(routes.canales+idCanal+'/'+viewer).update({coins: give}).then(function () {
        resolve();
      }, function (err) {
        reject();
      });
    });
  });
}

// remove Coins
firebaseI.removeCoins = function (viewer, cantidad, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales+idCanal+'/'+viewer).once('value', function (data) {
      userInfo = data.val();
      coins = userInfo.coins;
      give = coins - cantidad;
      db.ref(routes.canales+idCanal+'/'+viewer).update({coins: give}).then(function () {
        resolve();
      }, function (err) {
        reject();
      });
    });
  });
}

// bonus para viewer
firebaseI.bonus = function(username, cantidad, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales + idCanal + '/' + username).once('value', function(data) {
      if (data.val()) {
        userInf = data.val();
        coins = userInf.coins;
        cantidad = JSON.parse(cantidad);
        bonus = coins + cantidad;
        db.ref(routes.canales + idCanal + '/' + username).update({coins: bonus}).then(function() {
          resolve(true);
        });
      } else {
        reject()
      }
    });
  });
}

// bonus all
firebaseI.bonusAll = function(viewer, cantidad, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.canales + idCanal + '/' + viewer).once('value', function(data) {
      if (data.val()) {
        userInf = data.val();
        coins = userInf.coins;
      } else {
        coins = 0;
      }
      cantidadParse = JSON.parse(cantidad);
      bonus = coins + cantidadParse;
      db.ref(routes.canales + idCanal + '/' + viewer).update({coins: bonus}).then(function() {
        resolve(true);
      });
    }).catch(function(err) {
      console.log(err);
    });
  });
}

firebaseI.make = function (message, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.apuestas+idCanal).once('value', function (data) {
      if (!data.val()) {
        var saveOpcions = {titulo: message[1], opciones: []};
        for (var i = 2; i < message.length; i++) {
          opcion = String.fromCharCode(95 + i);
          valor = message[i];
          saveOpcions.opciones.push({opcion: opcion, valor: valor});
        }
        db.ref(routes.apuestas+idCanal).update(saveOpcions);
        resolve(saveOpcions);
      } else {
        reject('Tienes una apuesta activa!');
      }
    }, function (err) {
      reject('Ha ocurrido un problema, no se ha podido crear la apuesta!');
    });
  });
};

// apostar
firebaseI.apuesta = function (opcion, cantidad, viewer, idCanal) {
  cantidadParse = JSON.parse(cantidad);
  return new Promise(function(resolve, reject) {
    db.ref(routes.apuestas+idCanal+'/apostadores').orderByKey().equalTo(viewer).once('value', function (data) {
      if (data.val()) { // <-- saber si el viewer ya aposto
        reject('Tu ya has apostado!!');
      } else {
        if (cantidad >= 5) { // <-- la apuesta es maypr a 5
          firebaseI.checkCoins(viewer, idCanal).then(function (viewerCoins) {
            if (viewerCoins >= cantidad) { // <-- verificar coins del viewer
              // restar cantidad apostada a las coins del viewer
              firebaseI.removeCoins(viewer, cantidad, idCanal).then(function (response) {
                db.ref(routes.apuestas+idCanal+'/apostadores').update({ // <-- actualizar lista de apostadores
                  [viewer]: {
                    name: viewer,
                    cantidad: cantidadParse,
                    opcion
                  }
                }).then(function () {
                  db.ref(routes.apuestas+idCanal+'/fondo').once('value', function (data) { // optener datos del fondo
                    var fondoData = data.val();
                    if (!fondoData){ // si no hay info
                      cuantos = 0;
                      fondo = 1000;
                      var cuantosX = 0;
                      var cantidadX = 0;
                    } else if (fondoData.opciones[opcion]){ // si hay info
                      cuantos = fondoData.cuantos;
                      fondo = fondoData.fondo;
                      var cuantosX = fondoData.opciones[opcion].cuantos;
                      var cantidadX = fondoData.opciones[opcion].cantidad;
                    } else {
                      cuantos = fondoData.cuantos;
                      fondo = fondoData.fondo;
                      var cuantosX = 0;
                      var cantidadX = 0;
                    }
                    var newApostador = {
                      cuantos: cuantos+1,
                      fondo: fondo + cantidadParse
                    };
                    db.ref(routes.apuestas+idCanal+'/fondo').update(newApostador).then(function () { // <-- actualizar fondo de la apuesta
                      var updateOpcion = {
                        cuantos: cuantosX+1,
                        cantidad: cantidadX + cantidadParse
                      };
                      db.ref(routes.apuestas+idCanal+'/fondo/opciones/'+opcion).update(updateOpcion).then(function () {
                        resolve('todo salio bien');
                      }, function (err) {
                        reject('Ha ocurrio un error');
                      });
                    }, function (err) {
                      reject('Ha ocurrido un error! >> '+err); // <-- ocurrio un error al actualizar el fondo
                    });
                  });
                });
              });
            } else {
              reject('No tienes suficientes coins'); // <-- el viewer no tiene suficientes coins
            }
          }).catch(function (err) {
            console.log(err);
            reject('Ha ocurrido un error')
          });
        } else {
          reject('No seas codo, apuesta mas!'); // <-- la apuesta es menor a 5
        }
      }
    }).catch(function (err) {
      reject(err)
    });
  });
}

firebaseI.ganaApuesta = function (opcion, idCanal) {
  return new Promise(function(resolve, reject) {
    db.ref(routes.apuestas+idCanal+'/fondo').once('value', function (data) {
      var fondo = data.val();
      var opcionGanadora = fondo.opciones[opcion];
      if (!opcionGanadora) {
        opcionGanadora = {cantidad: 0};
        db.ref(routes.apuestas+idCanal).remove().then(function () {
          resolve('Se ha seleccionado la opcion '+opcion+' como la ganadora, ya les pague a quienes apostaron por ella.');
        }, function () {
          reject('Ha ocurrido un error');
        })
      }
      var premio = fondo.fondo;
      console.log('premio >>', premio);
      var total = premio - opcionGanadora.cantidad;
      if (total % 1 != 0) {
        total = Math.round(total-1);
      }
      console.log('total >>', total);
      db.ref(routes.apuestas+idCanal+'/apostadores').once('value', function (data) {
        var apostadores = data.val();
        for (var name in apostadores) {
          var aposto = apostadores[name].cantidad;
          console.log(name, 'aposto >>', aposto);
          var opcionSelect = apostadores[name].opcion[0];
          if (opcionSelect == opcion) {
            var gana = aposto+total;
            console.log('gana >>', gana);
            console.log(name);
            db.ref('channels/'+idCanal+'/'+name).once('value', function (response) {
              var oldCoins = response.val().coins;
              var newCoins = oldCoins+gana;
              db.ref('channels/'+idCanal+'/'+name).update({coins: newCoins}).then(function (response) {
                db.ref(routes.apuestas+idCanal).remove().then(function () {
                  resolve('Se ha seleccionado la opcion '+opcion+' como la ganadora, ya les pague a quienes apostaron por ella.');
                }, function () {
                  reject('Ha ocurrido un error');
                })
              }, function () {
                reject('Ha ocurrido un error');
              });
            });
          }
        }
      }).then(null, function () {
        reject('Ha ocurrido un error');
      });
    }).then(null, function () {
      reject('Ha ocurrido un error');
    });
  });
}

module.exports = firebaseI;
} catch(err) {
  console.log(err);
}
