const https = require('https');
const http = require('http');
const tmi = require('tmi.js');
const jsonfile = require('jsonfile');
const firebase = require('../models/firebaseInit.js');
const isMatch = require('is-match');

exports.bot = function(datos) {
  firebase.getData(datos.name).then(function (response) {
    var channelData = response;
  });

  canal = channelData.name;
  //canal = 'thedark_stroker';
  var idCanal = channelData._id;
  var viewersOn;
  var moderadorsOn;
  var numbers = isMatch(/[0-9]/g);

  function verificarNumeros(numeros) {
    return new Promise(function(resolve, reject) {
      if (numeros) {
        result = numbers(numeros);
        if (result) {
          resolve();
        } else {
          reject()
        }
      } else {
        reject()
      }
    });
  }

  // tmi config
  var options = {
    options: {
      debug: true
    },
    connection: {
      cluster: "aws",
      reconnect: true
    },
    identity: {
      username: "FrikBot",
      password: "oauth:saelgws5h3hnjhisvs98u06ntlrb8m"
    },
    channels: [canal]
  };

  // gamble
  var gamble = function() {
    ruleta = Math.floor((Math.random() * 100) + 1);
    console.log(ruleta);
    if (ruleta % 2) {
      firebase.removeCoins(username, cantidad, idCanal).then(function (response) {
        client.action(canal, displayName + ' veamos que tal tu suerte... Numero: ' + ruleta + '... Has perdido ' + cantidad + '!!');
      }, function (err) {
        client.action(canal, 'Ha ocurrido un error!');
      });
      /*db.ref('users/' + username).update({coins: coins}).then(function() {
          client.action(canal, displayName + ' veamos que tal tu suerte... Numero: ' + ruleta + '... Has perdido ' + cantidad + '!! ahora tienes ' + coins);
        });*/
    } else {
      firebase.addCoins(username, cantidad, idCanal).then(function (response) {
        client.action(canal, displayName + ' veamos que tal tu suerte... Numero: ' + ruleta + '... Has ganado ' + cantidad * 2 + '!!');
      }, function (err) {
        client.action(canal, 'Ha ocurrido un error!');
      });
      /*db.ref('users/' + username).update({coins: coins}).then(function() {
          client.action(canal, displayName + ' veamos que tal tu suerte... Numero: ' + ruleta + '... Has ganado ' + cantidad * 2 + '!! ahora tienes ' + coins);
        });*/
    }
  };

  // connect bot
  var client = new tmi.client(options);
  client.connect();

  // get channel data
  function loadView(canal) {
    return new Promise(function(resolve, reject) {
      https.get('https://tmi.twitch.tv/group/user/' + canal + '/chatters', (res) => {

        var buff = '';

        res.setEncoding('utf8');
        res.on('data', (d) => {
          buff += d;
        });
        res.on('end', () => {
          try {
            var parsedData = JSON.parse(buff);
            viewersOn = parsedData.chatters.viewers;
            moderadorsOn = parsedData.chatters.moderators;
            var totalViews = viewersOn.concat(moderadorsOn);
            if (totalViews) {
              resolve(totalViews);
            } else {
              reject('espera, estoy cargando la info');
              console.log('espera, estoy cargando la info');
            }
          } catch (e) {
            reject(e.message);
            console.error(e.message);
          }
        });

      }).on('error', (e) => {
        reject(e);
        console.error(e);
      });
    });
  };

  // on connect bot action
  client.on('connected', function(address, port) {
    client.action(canal, "Hola, soy FrikBot!");
  });

  // chat actions
  client.on('chat', function(channel, user, message, self) {
    username = user['username'];
    displayName = user['display-name'];
    message = message.toLowerCase();
    message = message.split(" ", 10);
    comando = message.slice(0, 1);

    if (comando == '!coins') {
      firebase.checkCoins(username, idCanal).then(function(response) {
        client.action(canal, displayName + ' tienes: ' + response + ' frikcoins');
      }, function (err) {
        client.action(canal, 'Hmmm... parece que aun no tienes coins... PanicVis');
      });
    }

    if (comando == '!gamble') {
      cantidad = message.slice(1);
      verificarNumeros(cantidad).then(function () {
        if (cantidad % 1 != 0) {
          cantidad = Math.round(cantidad-1);
        }
        if (cantidad >= 5) {
          firebase.checkCoins(username, idCanal).then(function(callback) {
            if (callback >= cantidad) {
              gamble();
            } else {
              client.action(canal, displayName + ' no tienes suficientes frikcoins BibleThump');
            }
          });
        } else {
          client.action(canal, displayName + ' no seas codo, apuesta mas TehePelo');
        }
      }, function () {
        client.action(canal, displayName + ' he chaval... seguro apostaste numeros?');
      });
    }

    if (comando == '!apuesta') {
      opcion = message.slice(1,2);
      cantidad = message.slice(2,3);
      verificarNumeros(cantidad).then(function () {
        if (cantidad % 1 != 0) {
          cantidad = Math.round(cantidad-1);
        }
        firebase.apuesta(opcion, cantidad, username, idCanal).then(function (response) {
          client.action(canal, displayName + ' has apostado '+cantidad+' coins por la opcion '+opcion);
        }, function (error) {
          client.action(canal, displayName + ' '+error);
        }, function () {
          client.action(canal, displayName + ' he chaval... seguro apostaste numeros?');
        });
      }).catch(function () {
        console.log(true);
        client.action(canal, displayName + ' recuerda usar !apuesta '+opcion+' <coins>');
      });
    }
    if (username == canal || username == 'osirisfrik') {
      if (comando == '!bonus') {
        user = message.slice(1, 2);
        cantidad = message.slice(2,3);
        console.log(user);
        if (cantidad != null && user != null) {
          firebase.bonus(user, cantidad, idCanal).then(function () {
            client.action(canal, 'Se le a dado un  bonus de '+cantidad+' coins a '+user+'.');
          }, function (err) {
            client.action(canal, displayName+' wop, ha ocurrido un error al dar el bonus');
          });
        } else {
          client.action(canal, displayName+' por favor ingresa un usuario y la cantidad');
        }
      }
      if (comando == '!bonusall') {
        cantidad = message.slice(1,2);
        cantidadCheck = numbers(cantidad);
        if (cantidadCheck) {
          loadView(canal).then(function (response) {
            for (var i = 0; i < response.length; i++) {
              firebase.bonusAll(response[i], cantidad, idCanal)
            }
            client.action(canal, 'Se a dado un bonus de '+cantidad+' a todos los presentes SwiftRage , cortecia de '+displayName)
          });
        }
      }
      if (comando == '!make') {
        var msjOpcions = null;
        firebase.make(message, idCanal).then(function (response) {
          for (var i = 0; i < response.opciones.length; i++) {
            if (msjOpcions != null) {
              msjOpcions += response.opciones[i].opcion+') '+response.opciones[i].valor+', ';
            } else {
              msjOpcions = response.opciones[i].opcion+') '+response.opciones[i].valor+', ';
            }
          }
          client.action(canal, 'Se ha creado la apuesta '+response.titulo+' con las opciones '+msjOpcions+'puedes botar con !apuesta <opcion> <apuesta>');
        }, function (err) {
          client.action(canal, displayName+' '+err);
        });
      }
      if (comando == '!gana') {
        opcion = message.slice(1, 2);
        firebase.ganaApuesta(opcion, idCanal).then(function (response) {
          client.action(canal, response);
        });
      }
    }
  });

  client.on('join', function(channel, username, self) {});

  setInterval(function() {
    loadView(canal).then(function (response) {
      channelId = datos._id;
      if (response) {
        for (var i = 0; i < response.length; i++) {
          firebase.addViewers(response[i], idCanal);
        }
      } else {
        console.log(false);
        console.log('-----');
      }
    });
  }, 60000);

};
