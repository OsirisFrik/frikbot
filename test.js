// modules
const http = require('http');
const https = require('https');
const express = require('express');
var session = require('express-session');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const path = require('path');
const tess = require('./models/tmi.js');
const jsonfile = require('jsonfile');
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const request = require('request');

// Define our constants, you will change these with your own
const TWITCH_CLIENT_ID = 'gu0dkmdupoxx5iquzyqj6lhe2auknt';
const TWITCH_SECRET = 't0zynyd24s47q3kfdz1j4l1dzs1595';
const SESSION_SECRET = 'popodegato';
const CALLBACK_URL = 'http://localhost:3000/auth/twitch/callback'; // You can run locally with - http://localhost:3000/auth/twitch/callback

// Local Config
var file = 'config.json';
var localConfig = jsonfile.readFileSync(file);

// localConfig Data
canal = localConfig.canal;
firebaseConfig = localConfig.firebase;
userData = localConfig.userData;

// user info
if (userData != null) {
  var username = userData.profile.name;
  var userPic = userData.profile.logo;
  var displayName = userData.profile.display_name;
} else {
  var username = null;
  var userPic = 'img/userDefault.png';
  var displayName = null;
}

// express app
var app = express();
app.use(session({secret: SESSION_SECRET, resave: false, saveUninitialized: false}));
app.use(express.static('public'));
app.use(passport.initialize());
app.use(passport.session());

// handlebars config
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
  defaultLayout: 'layout',
  //layoutsDir: "bot/views/layouts",
  extname: '.hbs',
  helpers: {
    userPic: userPic,
    displayName: displayName
  }
}));
app.set('view engine', '.hbs');

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

// bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Override passport profile function to get user profile from Twitch API
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
  var options = {
    url: 'https://api.twitch.tv/kraken/user',
    method: 'GET',
    headers: {
      'Client-ID': TWITCH_CLIENT_ID,
      'Accept': 'application/vnd.twitchtv.v5+json',
      'Authorization': 'OAuth ' + accessToken
    }
  };

  request(options, function(error, response, body) {
    if (response && response.statusCode == 200) {
      done(null, JSON.parse(body));
    } else {
      done(JSON.parse(body));
    }
  });
};

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use('twitch', new OAuth2Strategy({
  authorizationURL: 'https://api.twitch.tv/kraken/oauth2/authorize',
  tokenURL: 'https://api.twitch.tv/kraken/oauth2/token',
  clientID: TWITCH_CLIENT_ID,
  clientSecret: TWITCH_SECRET,
  callbackURL: CALLBACK_URL,
  state: true
}, function(accessToken, refreshToken, profile, done) {
  profile.accessToken = accessToken;
  profile.refreshToken = refreshToken;

  var userData = {
    userData: {
      profile
    }
  };

  jsonfile.writeFileSync(file, userData, {
    spaces: 2
  }, function(err) {
    console.error(err)
  });

  // Securely store user profile in your DB
  //User.findOrCreate(..., function(err, user) {
  //  done(err, user);
  //});
  done(null, profile);
}));

// Set route to start OAuth link, this is where you define scopes to request
app.get('/auth/twitch', passport.authenticate('twitch', {scope: 'user_read'}));

// Set route for OAuth redirect
app.get('/auth/twitch/callback', passport.authenticate('twitch', {
  successRedirect: '/',
  failureRedirect: '/'
}));

app.get('/login', function(req, res) {
  res.render('login', {title: 'Login'});
});

app.get('/logout', function (req, res) {
  req.logout();
  resetData = {
    userData: null,
    firebaseConfig: null
  };
  jsonfile.writeFileSync(file, resetData, {
    spaces: 2
  }, function(err) {
    console.error(err)
  });
  res.render('logout');
});

if (!session && !session.passport && !session.passport.user || !userData) {
  app.get('/', function (req, res) {
    res.redirect('/login');
  });
  app.get('/config', function (req, res) {
    res.redirect('/login');
  });
} else if (firebaseConfig == null){
  app.get('/', function (req, res) {
    res.redirect('/config');
  });
  app.get('/config', function(req, res) {
    res.render('config', {title: 'Configuracion'});
  });
} else {
  app.get('/', function (req, res) {
    res.render('index', {title: 'Home'});
  });

  app.get('/config', function(req, res) {
    res.render('config', {
      title: 'Configuracion',
      canal: username,
      apiKey: firebaseConfig.apiKey,
      authDomain: firebaseConfig.authDomain,
      databaseURL: firebaseConfig.databaseURL,
      projectId: firebaseConfig.projectId,
      storageBucket: firebaseConfig.storageBucket,
      messagingSenderId: firebaseConfig.messagingSenderId
    });
  });

  app.get('/bet', function(req, res) {
    res.render('bet', {title: 'Apuestas'});
  });
}

app.post('/config', function(req, res, next) {
  var saveConfig = {
    userData: userData,
    firebase: {
      apiKey: req.body.apiKey,
      authDomain: req.body.authDomain,
      databaseURL: req.body.databaseURL,
      projectId: req.body.projectId,
      storageBucket: req.body.storageBucket,
      messagingSenderId: req.body.messagingSenderId
    }
  };
  jsonfile.writeFileSync(file, saveConfig, {
    spaces: 2
  }, function(err) {
    console.error(err)
  });
  res.redirect('/');
});

app.listen(3000, function() {
  console.log('Server corriendo en http://localhost:3000');
});
